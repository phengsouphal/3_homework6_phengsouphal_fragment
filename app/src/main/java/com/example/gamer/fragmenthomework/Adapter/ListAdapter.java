package com.example.gamer.fragmenthomework.Adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.gamer.fragmenthomework.Callback.OnItemClickListener;
import com.example.gamer.fragmenthomework.Entity.UniversityData;
import com.example.gamer.fragmenthomework.R;

import java.util.List;

public class ListAdapter extends RecyclerView.Adapter {

    private List<UniversityData> universityDataList;
    private OnItemClickListener onItemClickListener;

    private TextView title, phone, web, email, location;
    private ImageView imageView, inImageView;
    public ListAdapter(List<UniversityData> universityDataList) {
        this.universityDataList = universityDataList;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_items, viewGroup, false);
        onItemClickListener= (OnItemClickListener) viewGroup.getContext();
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {

        UniversityData data=universityDataList.get(i);
        title.setText(data.getTitle());
        phone.setText(data.getPhone());
        web.setText(data.getWeb());
        email.setText(data.getEmail());
        location.setText(data.getAddress());
        imageView.setImageResource(data.getPicture());
        inImageView.setImageResource(data.getPicture());
    }

    @Override
    public int getItemCount() {
        return universityDataList.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {


        public MyViewHolder(View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.tv_un_name);
            phone = itemView.findViewById(R.id.tv_un_phone);
            web = itemView.findViewById(R.id.tv_un_web);
            email = itemView.findViewById(R.id.tv_un_email);
            location = itemView.findViewById(R.id.tv_un_address);
            imageView = itemView.findViewById(R.id.ivUniversity);
            inImageView = itemView.findViewById(R.id.iv_in_University);
            itemView.setOnClickListener(this);
        }


        @Override
        public void onClick(View v) {
            int i=getAdapterPosition();
            UniversityData data=universityDataList.get(i);
            onItemClickListener.onGetItems(data);
        }
    }
}