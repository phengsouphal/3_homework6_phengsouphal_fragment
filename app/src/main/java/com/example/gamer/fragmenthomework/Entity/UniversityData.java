package com.example.gamer.fragmenthomework.Entity;

public class UniversityData {
    private String title;
    private String phone;
    private String web;
    private String email;
    private String address;
    private int picture;


    public UniversityData(String title, String phone, String web, String email, String address, int picture) {
        this.title = title;
        this.phone = phone;
        this.web = web;
        this.email = email;
        this.address = address;
        this.picture = picture;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getWeb() {
        return web;
    }

    public void setWeb(String web) {
        this.web = web;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getPicture() {
        return picture;
    }

    public void setPicture(int picture) {
        this.picture = picture;
    }
}
