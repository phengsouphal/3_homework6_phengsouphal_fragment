package com.example.gamer.fragmenthomework.Callback;

import com.example.gamer.fragmenthomework.Entity.UniversityData;

public interface OnItemClickListener {
    void onGetItems(UniversityData universityData);
}
