package com.example.gamer.fragmenthomework;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.gamer.fragmenthomework.Entity.UniversityData;


/**
 * A simple {@link Fragment} subclass.
 */
public class DetailFragment extends Fragment {

    private UniversityData data;
    static DetailFragment detailFragment;
    private TextView title,phone,web,email,location;
    private ImageView imageView,inImageView;
    public static DetailFragment newInstance(){
        if (detailFragment==null)
            detailFragment=new DetailFragment();

        return detailFragment;

    }

    public void setData(UniversityData universityData){
       data=universityData;
    }

    public DetailFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_detail, container, false);

        title=view.findViewById(R.id.tv_un_name);
        phone=view.findViewById(R.id.tv_un_phone);
        web=view.findViewById(R.id.tv_un_web);
        email=view.findViewById(R.id.tv_un_email);
        location=view.findViewById(R.id.tv_un_address);
        imageView=view.findViewById(R.id.ivUniversity);
        inImageView=view.findViewById(R.id.iv_in_University);


    return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (data!=null){
            title.setText(data.getTitle());
            phone.setText(data.getPhone());
            web.setText(data.getWeb());
            email.setText(data.getEmail());
            imageView.setImageResource(data.getPicture());


        }
    }
}
