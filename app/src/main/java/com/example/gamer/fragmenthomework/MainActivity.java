package com.example.gamer.fragmenthomework;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.example.gamer.fragmenthomework.Callback.OnItemClickListener;
import com.example.gamer.fragmenthomework.Entity.UniversityData;

public class MainActivity extends AppCompatActivity implements OnItemClickListener {


    static final String DEFAULT_FRAGMENT="DEFAULT_FRAGMENT";
    static final String DETAIL_FRAGMENT="DETAIL_FRAGMENT";
    FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

       fragmentManager=getSupportFragmentManager();
       fragmentTransaction=fragmentManager.beginTransaction();
       Fragment fragment=DefualtFragment.newInstance();
       addFragment(fragment);


    }

    private void addFragment(Fragment fragment){
        fragmentTransaction=fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fragment_container,fragment);
        if(fragment instanceof DetailFragment)
        {
            fragmentTransaction.addToBackStack(null);
        }
        fragmentTransaction.commit();
    }




    @Override
    public void onGetItems(UniversityData universityData) {
        DetailFragment fragment=DetailFragment.newInstance();
        fragment.setData(universityData);
        addFragment(fragment);


    }

}
