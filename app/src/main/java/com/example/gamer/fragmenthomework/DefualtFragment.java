package com.example.gamer.fragmenthomework;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.example.gamer.fragmenthomework.Adapter.ListAdapter;
import com.example.gamer.fragmenthomework.Entity.UniversityData;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class DefualtFragment extends Fragment {

    static DefualtFragment defualtFragment;
    private RecyclerView recyclerView;
    List<UniversityData> universityData;

    public static DefualtFragment newInstance(){
        if (defualtFragment==null)
            defualtFragment=new DefualtFragment();
        return defualtFragment;
    }


    public DefualtFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_defualt, container, false);
        ListAdapter listAdapter=new ListAdapter(getListData());
        recyclerView=view.findViewById(R.id.recyclerView);
        recyclerView.setAdapter(listAdapter);
        RecyclerView.LayoutManager layoutManager=new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);

       return  view;

    }

    List<UniversityData>getListData(){
        List<UniversityData> universityDataList=new ArrayList<>();

        universityDataList.add(new UniversityData("RUPP","099 111 222","WWW.RUPP.COM","RUPP@gmail.com","Phnom penh",R.drawable.rupp));
        universityDataList.add(new UniversityData("SETEC","099 333 333","WWW.SETEC.COM","SETEC@gmail.com","Phnom penh",R.drawable.setec));
        universityDataList.add(new UniversityData("RULE","099 444 444","WWW.RULE.COM","RULE@gmail.com","Phnom penh",R.drawable.rule));
        universityDataList.add(new UniversityData("NU","099 555 555","WWW.NU.COM","NU@gmail.com","Phnom penh",R.drawable.nu));
        universityDataList.add(new UniversityData("WASTERN","099 666 666","WWW.WASTERN.COM","WASTERN@gmail.com","Phnom penh",R.drawable.download));
        universityDataList.add(new UniversityData("PPIU","099 777 777","WWW.PPIU.COM","PPIU@gmail.com","Phnom penh",R.drawable.web));


        return universityDataList;
    }



}
